# -*- coding : utf-8 -*-

__name__ = "kami"
__title__ = "kamilib"
# PiPy == "0.1.2a"
# testPiPy == "0.1.1b45"
__version__ = "0.1.2"
__licence__ = "MIT"
__doc__ = "HTR / OCR models evaluation agnostic Python package, originally based on the Kraken transcription system."